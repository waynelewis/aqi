# Purple Air AQI readout

## Usage

Running the `aqi` script will get the live data from the sensor.

To get periodic updates, use:

```console
$ watch -t -n 5 ./aqi
```

